from django.db import models
from django.utils.translation import ugettext as _
from django.shortcuts import reverse
from django.conf import settings


class Post(models.Model):

    posted_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE)

    description = models.TextField(
        _("Post"),
        max_length=500)

    post_date = models.DateTimeField(
        _("post_date"),
        auto_now=True)

    def __str__(self):
        return self.description

    # def get_absolute_url(self):
    #     return reverse("_detail", kwargs={"pk": self.pk})


class Comment(models.Model):

    commentor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_(""),
        on_delete=models.CASCADE)

    comment = models.TextField(
        _("comment"),
        max_length=255)

    comment_post = models.ForeignKey(
        "Post",
        verbose_name=_(""),
        on_delete=models.CASCADE)

    comment_date = models.DateTimeField(
        _("comment_date"),
        auto_now=True)

    def __str__(self):
        return self.comment
