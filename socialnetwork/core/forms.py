# from django import forms
# from django.conf import settings

from apps.accounts.forms import UserCreationForm, CustomUser


class SignUpForm(UserCreationForm):

    class Meta:
        model = CustomUser
